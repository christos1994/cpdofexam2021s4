package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class TestMethodTest {

    @Test
    public void testGstrWithNonNullString() {
        String inputString = "Test";
        TestMethod testMethod = new TestMethod(inputString);
        assertEquals(inputString, testMethod.gstr());
    }

    @Test
    public void testGstrWithNullString() {
        TestMethod testMethod = new TestMethod(null);
        assertNull(testMethod.gstr());
    }
}

