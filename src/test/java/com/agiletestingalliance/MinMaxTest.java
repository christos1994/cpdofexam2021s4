package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class MinMaxTest {

    @Test
    public void testFindMax() {
        MinMax minMax = new MinMax();
         assertEquals(5, minMax.findMax(3, 5));
         assertEquals(7, minMax.findMax(7, 4));
         assertEquals(10, minMax.findMax(10, 10));
            }
    @Test
    public void testBar() {
         MinMax minMax = new MinMax();
         assertEquals("Hello", minMax.bar("Hello"));
         assertEquals("", minMax.bar(""));
         assertEquals("", minMax.bar(null));
             }
    }
        
